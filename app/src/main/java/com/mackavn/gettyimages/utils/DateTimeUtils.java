package com.mackavn.gettyimages.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Class used to work with date and time
 */
public class DateTimeUtils {
    public static final String ISO_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ssZ";
    public static final String SDF_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";
    public static final String SIMPLE_TIME = "HH:mm";

    public String getCurrentTimeString() {
        Calendar today = Calendar.getInstance();
        return formatAndroidTime(today);
    }

    public String format(Calendar date, SimpleDateFormat format) {
        return format.format(date.getTime());
    }

    public Calendar parse(String s, SimpleDateFormat format) {
        try {
            Date date = format.parse(s);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            return calendar;
        } catch (ParseException e) {
            return null;
        }
    }

    private String formatAndroidTime(Calendar date) {
        SimpleDateFormat timeFormat = new SimpleDateFormat(SIMPLE_TIME, Locale.getDefault());
        timeFormat.setCalendar(date);
        return timeFormat.format(date.getTime());
    }
}
