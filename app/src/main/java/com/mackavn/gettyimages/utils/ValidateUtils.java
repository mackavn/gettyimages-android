package com.mackavn.gettyimages.utils;

import android.widget.TextView;

import javax.inject.Inject;

/**
 * Class used to validate any resources in any way
 */
public class ValidateUtils {

    @Inject
    public ValidateUtils() {

    }

    public String nullToString(String nullToString) {
        return nullToString != null ? nullToString : "";
    }

    public boolean validMail(String email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(nullToString(email)).matches();
    }

    public String tvToString(TextView view) {
        return view != null ? nullToString(view.getText().toString()) : "";
    }
}
