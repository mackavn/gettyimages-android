package com.mackavn.gettyimages.mvp_implementation.presenter;

import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.OnLifecycleEvent;
import android.content.Intent;
import android.util.Log;
import android.view.MenuItem;

import com.mackavn.gettyimages.common.AppError;
import com.mackavn.gettyimages.di.AppComponent;
import com.mackavn.gettyimages.mvp_interfaces.model.IBaseModel;
import com.mackavn.gettyimages.mvp_interfaces.presenter.IBasePresenter;
import com.mackavn.gettyimages.mvp_interfaces.view.IBaseView;

import java.lang.ref.WeakReference;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Mike Diachenko
 * on 09-Sep-17.
 * with dell latitude
 */

abstract class BasePresenter<T extends IBaseView, M extends IBaseModel>
        implements IBasePresenter.View, IBasePresenter.Model, LifecycleObserver {

    private WeakReference<T> view;
    private M model;
    private CompositeDisposable compositeDisposable;

    //region view actions

    public BasePresenter(T view, M model) {
        view.getLifecycle().addObserver(this);
        this.model = model;
        compositeDisposable = new CompositeDisposable();
        if (getModel() != null) {
            getModel().setPresenter(this);
        }
    }

    /*
     * Lifecycle methods block
     */

    /**
     * In onCreate method we initialize compositeDisposable
     * and setting models presenter
     */

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    protected void onCreate() {
        Log.d("testPresenter", "onCreate: ");
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    protected void onStart() {
        //override if necessary
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    protected void onResume() {
        //override if necessary
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    protected void onPause() {
        //override if necessary
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    protected void onStop() {
        //override if necessary
    }

    /**
     * In onDestroy we clear compositedisposable, and null it
     */
    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    protected void onDestroy() {
        //override if necessary
        compositeDisposable.clear();
        compositeDisposable = null;
        view = null;
    }

    /**
     * Passed from activity to presenter
     *
     * @param request - request code
     * @param result  - result code (Activity constants OK\CANCEL)
     * @param data    - intent data from result
     */
    @Override
    public void onActivityResult(int request, int result, Intent data) {
        //override if necessary
    }

    /**
     * Passed from activity to presenter
     *
     * @param item - item selected as option
     */
    @Override
    public void onOptionsItemSelected(MenuItem item) {
        //override if necessary
    }

    /**
     * Setting view to cooperate with
     *
     * @param view view to work in presenter
     */
    @SuppressWarnings("unchecked")
    @Override
    public void setView(IBaseView view) {
        try {
            this.view = new WeakReference<>((T) view);
        } catch (Exception e) {
            throw new IllegalArgumentException("Pass correct view, please");
        }
    }

    @Override
    public void resolve() {
        if (compositeDisposable != null) {
            compositeDisposable.clear();
        }
        compositeDisposable = null;
        if (getModel() != null) {
            getModel().resolve();
        }
    }

    //endregion

    //region model actions

    /**
     * Error received from model
     * By default - passed to activity
     *
     * @param error - error object
     */
    @Override
    public void onError(AppError error) {
        if (getView() != null) {
            getView().showError(error);
            getView().unlockUI();
        }
    }

    //endregion

    //region own methods

    /**
     * Return T view if can
     *
     * @return either T object or null
     */
    protected T getView() {
        return view != null ? view.get() : null;
    }

    /**
     * Return M model instance
     *
     * @return model
     */
    protected M getModel() {
        return model;
    }

    /**
     *
     * @return compositedisposable to work with disposables
     */
    protected CompositeDisposable getCompositeDisposable() {
        return compositeDisposable;
    }

    //endregion
}
