package com.mackavn.gettyimages.mvp_implementation.view.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mackavn.gettyimages.App;
import com.mackavn.gettyimages.common.AppError;
import com.mackavn.gettyimages.di.ComponentsHolder;
import com.mackavn.gettyimages.mvp_interfaces.BaseFragmentEventListener;
import com.mackavn.gettyimages.mvp_interfaces.presenter.IBasePresenter;
import com.mackavn.gettyimages.mvp_interfaces.view.IBaseView;

import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by llaer on 09-Sep-17.
 */

public abstract class BaseFragment<T extends IBasePresenter.View> extends Fragment implements IBaseView {

    private boolean lockedUI;
    private ProgressDialog progressDialog;
    private BaseFragmentEventListener baseFragmentEventListener;
    private Unbinder unbinder;

    //region base methods

    /*
     * block of lifecycle methods
     */

    /**
     * In onAttach we trying to receive baseFragmentEventListener object
     *
     * @param context context that fragment attached to
     */
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof BaseFragmentEventListener) {
            baseFragmentEventListener = ((BaseFragmentEventListener) getContext());
        }
    }

    /**
     * In onCreate we processArgs, bind views, init progress
     *
     * @param savedInstanceState parameter that keep saved state after recreate activity
     */
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        processArguments();
        initProgressDialog();
        initComponent();
        setRetainInstance(isRetain());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(getLayoutResId(), container, false);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (((App) getActivity().getApplication()).getComponentsHolder() != null) {
            ((App) getActivity().getApplication()).getComponentsHolder().cleanComponents(getTag());
        }
        if (getPresenter() != null) {
            getPresenter().resolve();
        }
        if (unbinder != null) {
            unbinder.unbind();
            unbinder = null;
        }
    }

    /**
     * Clear baseFragmentEventListener
     */
    @Override
    public void onDetach() {
        baseFragmentEventListener = null;
        super.onDetach();
    }

    //endregion

    //region view actions

    /**
     * Override this method to show error in any way (snack\toast\popup\screen)
     *
     * @param error custom project error class that contains code, message and throwable
     */
    @Override
    public void showError(AppError error) {
        Log.d("BaseFragment", "Error " + error);
    }

    @Override
    public void showProgressBar() {
        if (progressDialog != null) {
            progressDialog = new ProgressDialog(getContext());
        }
    }

    @Override
    public void showProgressView() {
        if (getProgressView() != null) {
            getProgressView().setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void showEmptyView() {
        if (getEmptyView() != null) {
            getEmptyView().setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void hideProgress() {
        if (getProgressView() != null) {
            getProgressView().setVisibility(View.GONE);
        }
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    @Override
    public void lockUI() {
        lockedUI = true;
    }

    @Override
    public boolean isUILocked() {
        return lockedUI;
    }

    @Override
    public void unlockUI() {
        lockedUI = false;
    }

    @Override
    public void launchActivity(Class clzz, boolean finish) {
        if (baseFragmentEventListener != null) {
            baseFragmentEventListener.launchActivity(clzz, finish);
        }
    }

    @Override
    public void launchIntent(Intent intent, boolean finish) {
        if (baseFragmentEventListener != null) {
            baseFragmentEventListener.launchIntent(intent, finish);
        }
    }

    @Override
    public void openFragment(BaseFragment fragment, boolean addToStack) {
        if (baseFragmentEventListener != null) {
            baseFragmentEventListener.openFragment(fragment, addToStack);
        }
    }

    @Override
    public void hideEmptyView() {
        if (getEmptyView() != null) {
            getEmptyView().setVisibility(View.GONE);
        }
    }

    //endregion

    //region own methods

    protected View getProgressView() {
        //override to use progressView mechanism
        return null;
    }

    protected View getEmptyView() {
        //override to use emptyView mechanism
        return null;
    }

    /**
     * Initing progressDialog to be used with showProgressBar() method
     */
    private void initProgressDialog() {
        if (getProgressTitle() == null && getProgressMessage() == null) {
            return;
        }
        progressDialog = new ProgressDialog(getContext());
        if (getProgressTitle() != null) {
            progressDialog.setTitle(getProgressTitle());
        }
        if (getProgressMessage() != null) {
            progressDialog.setMessage(getProgressMessage());
        }
    }

    protected String getProgressTitle() {
        //override to show progressDialog title
        return "";
    }

    protected String getProgressMessage() {
        //override to show progressDialog body
        return "";
    }

    protected boolean isRetain() {
        return true;
    }

    protected ComponentsHolder getComponentHolder() {
        return ((App) getActivity().getApplication()).getComponentsHolder();
    }

    /**
     * This method used to process arguments and extract data from it
     */
    protected void processArguments() {
        //override if necessary
    }

    //endregion

    //region abstract methods

    protected abstract int getLayoutResId();

    protected abstract T getPresenter();

    protected abstract void initComponent();

    //endregion
}
