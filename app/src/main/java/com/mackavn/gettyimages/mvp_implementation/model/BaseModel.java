package com.mackavn.gettyimages.mvp_implementation.model;

import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.OnLifecycleEvent;

import com.mackavn.gettyimages.common.AppError;
import com.mackavn.gettyimages.di.AppComponent;
import com.mackavn.gettyimages.mvp_interfaces.model.IBaseModel;
import com.mackavn.gettyimages.mvp_interfaces.presenter.IBasePresenter;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Mike Diachenko
 * on 09-Sep-17.
 * with dell latitude
 */

abstract class BaseModel<T extends IBasePresenter.Model> implements IBaseModel, LifecycleObserver {

    private T presenter;
    private CompositeDisposable compositeDisposable;

    public BaseModel(Lifecycle lifecycle) {
        lifecycle.addObserver(this);
        compositeDisposable = new CompositeDisposable();
    }

    //region view actions

    /*
     * block of lifecycle methods
     */

    /**
     * in onCreate we initialize our compositeDisposable that will handle all model ctions
     */

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    protected void onCreate() {
        //override if necessary
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    protected void onStart() {
        //override if necessary
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    protected void onResume() {
        //override if necessary
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    protected void onPause() {
        //override if necessary
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    protected void onStop() {
        //override if necessary
    }

    /**
     * In onDestroy we clear compositedisposable, and null it
     */
    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    protected void onDestroy() {
        //override if necessary
    }
    /**
     * Here we setting presenter, and throw IllegalArgumentException if it passed wrong
     *
     * @param presenter our presenter
     */
    @SuppressWarnings("unchecked")
    public void setPresenter(IBasePresenter.Model presenter) {
        try {
            this.presenter = (T) presenter;
        } catch (Exception e) {
            throw new IllegalArgumentException("pass correct presenter");
        }
    }

    @Override
    public void resolve() {
        if (compositeDisposable != null) {
            compositeDisposable.clear();
        }
        compositeDisposable = null;
        this.presenter = null;
    }

    //endregion

    //region own methods
    protected T getPresenter() {
        return presenter;
    }

    public CompositeDisposable getCompositeDisposable() {
        return compositeDisposable;
    }

    /**
     * Base error processing, that create AppError object from throwable and pass it to presenter
     *
     * @param throwable throwable to process
     */
    protected void processError(Throwable throwable) {
        if (getPresenter() != null) {
            AppError error = new AppError();
            error.setThrowable(throwable);
            getPresenter().onError(error);
        }
    }

    //endregion

}
