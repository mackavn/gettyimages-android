package com.mackavn.gettyimages.mvp_implementation.model;

import android.arch.lifecycle.Lifecycle;

import com.mackavn.gettyimages.data.network.api.interfaces.ImagesApiRepository;
import com.mackavn.gettyimages.data.storage.interfaces.HistoryStorageRepository;
import com.mackavn.gettyimages.entities.GettyItem;
import com.mackavn.gettyimages.entities.HistoryItem;
import com.mackavn.gettyimages.mvp_interfaces.model.IMainModel;
import com.mackavn.gettyimages.mvp_interfaces.presenter.IMainPresenter;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by mkavu on 08.02.2018.
 */

public class MainModel extends BaseModel<IMainPresenter.Model>
        implements IMainModel {

    private ImagesApiRepository imagesApiRepository;
    private HistoryStorageRepository historyStorageRepository;

    public MainModel(Lifecycle lifecycle,
                     ImagesApiRepository imagesApiRepository,
                     HistoryStorageRepository historyStorageRepository) {
        super(lifecycle);
        this.imagesApiRepository = imagesApiRepository;
        this.historyStorageRepository = historyStorageRepository;
    }

    //region model actions
    @Override
    public void searchImages(String searchQuery) {
        getCompositeDisposable().add(imagesApiRepository.getImages(searchQuery)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(gettyItem -> processImagesReceived(gettyItem, searchQuery), this::processError));
    }

    @Override
    public void saveSearchHistoryItem(String uri, String searchQuery) {
        getCompositeDisposable().add(historyStorageRepository.saveHistoryItem(new HistoryItem(searchQuery, uri))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError(this::processError)
                .subscribe());
    }

    @Override
    public void loadHistory() {
        getCompositeDisposable().add(historyStorageRepository.getHistoryItems()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::processHistoryItemsLoaded, this::processError));
    }

    //endregion

    //region own methods

    private void processHistoryItemsLoaded(List<HistoryItem> historyItems) {
        if (getPresenter() != null) {
            getPresenter().onHistoryItemsLoaded(historyItems);
        }
    }

    private void processImagesReceived(GettyItem item, String searchQuery) {
        if (getPresenter() != null) {
            getPresenter().onImagesReceived(item, searchQuery);
        }
    }

    //endregion
}
