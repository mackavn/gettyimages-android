package com.mackavn.gettyimages.mvp_implementation.view.activities;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.mackavn.gettyimages.App;
import com.mackavn.gettyimages.R;
import com.mackavn.gettyimages.mvp_interfaces.presenter.IMainPresenter;
import com.mackavn.gettyimages.mvp_interfaces.view.IMainView;
import com.mackavn.gettyimages.views.HistoryImagesAdapter;
import com.mackavn.gettyimages.views.SearchImagesAdapter;

import javax.inject.Inject;

import butterknife.BindView;

/**
 * Created by mkavu on 08.02.2018.
 */

public class MainActivity extends BaseActivity<IMainPresenter.View> implements IMainView {
    private static final String TAG = MainActivity.class.getSimpleName();

    @Inject IMainPresenter.View presenter;

    @BindView(R.id.recycler_view) RecyclerView recyclerView;
    @BindView(R.id.progress_view) ProgressBar progressView;

    //region base methods
    @Override
    public String getUniqueTag() {
        return TAG;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_main;
    }

    @Override
    protected IMainPresenter.View getPresenter() {
        return presenter;
    }

    @Override
    protected void initComponent() {
        ((App) getApplication())
                .getComponentsHolder()
                .getMainComponentByName(TAG, this)
                .inject(this);
    }

    @Override
    protected void initView(Bundle savedInstanceState) {

    }

    //endregion

    //region superclass methods

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);

        MenuItem searchItem = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) searchItem.getActionView();
        searchItem.setOnActionExpandListener(new MenuItem.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem menuItem) {
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem menuItem) {
                if (getPresenter() != null) {
                    getPresenter().onSearchActionsCollapse();
                }
                return true;
            }
        });
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                processSearchClick(s);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });

        return true;
    }

    @Override
    public void showProgressView() {
        super.showProgressView();
        progressView.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
    }

    @Override
    public void hideProgress() {
        super.hideProgress();
        progressView.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
    }

    //endregion

    //region view actions

    @Override
    public void showSearchImages(SearchImagesAdapter searchImagesAdapter) {
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(searchImagesAdapter);
    }

    @Override
    public void showHistoryImages(HistoryImagesAdapter historyImagesAdapter) {
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(historyImagesAdapter);
    }

    //endregion

    //region own methods

    private void processSearchClick(String searchQuery) {
        if (getPresenter() != null) {
            getPresenter().onSearchClick(searchQuery);
        }
    }

    //endregion
}
