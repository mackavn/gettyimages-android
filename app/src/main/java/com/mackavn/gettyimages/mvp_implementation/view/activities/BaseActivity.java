package com.mackavn.gettyimages.mvp_implementation.view.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.mackavn.gettyimages.App;
import com.mackavn.gettyimages.R;
import com.mackavn.gettyimages.common.AppError;
import com.mackavn.gettyimages.di.ComponentsHolder;
import com.mackavn.gettyimages.mvp_implementation.view.fragments.BaseFragment;
import com.mackavn.gettyimages.mvp_interfaces.BaseFragmentEventListener;
import com.mackavn.gettyimages.mvp_interfaces.presenter.IBasePresenter;
import com.mackavn.gettyimages.mvp_interfaces.view.IBaseView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Base activity class (instance of IView)
 */

public abstract class BaseActivity<T extends IBasePresenter.View> extends AppCompatActivity
        implements IBaseView, BaseFragmentEventListener {

    @BindView(R.id.toolbar) @Nullable Toolbar toolbar;
    @BindView(R.id.coordinator) @Nullable CoordinatorLayout coordinator;

    protected ActionBar actionBar;

    private boolean lockedUI; //use this flag to control UI, if it need to be locked on some actions
    private ProgressDialog progressDialog;

    //region base methods

    /*
     * block of lifecycle methods
     */

    /**
     * In onCreate we processIntent, bind views, init progress, init view,
     * init actionbar and tell presenter about ready state
     *
     * @param savedInstanceState parameter that keep saved state after recreate activity
     */
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResId());
        processIntent();
        initComponent();
        ButterKnife.bind(this);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            actionBar = getSupportActionBar();
        }
        initView(savedInstanceState);
        initProgressDialog();
        if (getPresenter() != null) {
            getPresenter().setView(this);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (isFinishing()) {
            if (((App) getApplication()).getComponentsHolder() != null) {
                ((App) getApplication()).getComponentsHolder().cleanComponents(getUniqueTag());
            }
            if (getPresenter() != null) {
                getPresenter().resolve();
            }
        }
    }

    //endregion

    //region view actions

    /**
     * Override this method to show error in any way (snack\toast\popup\screen)
     *
     * @param error custom project error class that contains code, message and throwable
     */
    @Override
    public void showError(AppError error) {
        Log.d("BaseFragment", "Error " + error.getThrowable().getMessage());
    }

    @Override
    public void showProgressBar() {
        if (progressDialog != null) {
            progressDialog = new ProgressDialog(this);
        }
    }

    @Override
    public void showProgressView() {
        if (getProgressView() != null) {
            getProgressView().setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void showEmptyView() {
        if (getEmptyView() != null) {
            getEmptyView().setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void hideProgress() {
        if (getProgressView() != null) {
            getProgressView().setVisibility(View.GONE);
        }
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    @Override
    public void lockUI() {
        lockedUI = true;
    }

    @Override
    public boolean isUILocked() {
        return lockedUI;
    }

    @Override
    public void unlockUI() {
        lockedUI = false;
    }

    @Override
    public void launchActivity(Class clzz, boolean finish) {
        startActivity(new Intent(this, clzz));
        if (finish) {
            finish();
        }
    }

    @Override
    public void launchIntent(Intent intent, boolean finish) {
        startActivity(intent);
        if (finish) {
            finish();
        }
    }

    @Override
    public void hideEmptyView() {
        if (getEmptyView() != null) {
            getEmptyView().setVisibility(View.GONE);
        }
    }

    //endregion

    //region fragment callbacks

    @Override
    public void openFragment(BaseFragment fragment, boolean addToBackStack) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        if (addToBackStack) {
            fragmentTransaction.addToBackStack(fragment.getUniqueTag());
        }
        fragmentTransaction.replace(R.id.fragment_container, fragment, fragment.getUniqueTag()).commit();
    }

    //endregion

    //region own methods

    protected View getProgressView() {
        //override to use progressView mechanism
        return null;
    }

    protected View getEmptyView() {
        //override to use emptyView mechanism
        return null;
    }

    /**
     * Initing progressDialog to be used with showProgressBar() method
     */
    private void initProgressDialog() {
        if (getProgressTitle() == null && getProgressMessage() == null) {
            return;
        }
        progressDialog = new ProgressDialog(this);
        if (getProgressTitle() != null) {
            progressDialog.setTitle(getProgressTitle());
        }
        if (getProgressMessage() != null) {
            progressDialog.setMessage(getProgressMessage());
        }
    }

    protected String getProgressTitle() {
        //override to show progressDialog title
        return "";
    }

    protected String getProgressMessage() {
        //override to show progressDialog body
        return "";
    }

    protected ComponentsHolder getComponentHolder() {
        return ((App) getApplication()).getComponentsHolder();
    }

    /**
     * This method used to process intents and extract data from it
     */
    protected void processIntent() {
        //override if necessary
    }

    //endregion

    //region abstract methods

    protected abstract int getLayoutResId();

    protected abstract T getPresenter();

    protected abstract void initComponent();

    /**
     * In this method you should init views, set listeners and other.
     *
     * @param savedInstanceState passed from onCreate object that can keep saved view state
     */
    protected abstract void initView(Bundle savedInstanceState);

    //endregion
}
