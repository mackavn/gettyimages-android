package com.mackavn.gettyimages.mvp_implementation.presenter;

import com.mackavn.gettyimages.entities.GettyItem;
import com.mackavn.gettyimages.entities.HistoryItem;
import com.mackavn.gettyimages.entities.ImagesItem;
import com.mackavn.gettyimages.mvp_interfaces.model.IMainModel;
import com.mackavn.gettyimages.mvp_interfaces.presenter.IMainPresenter;
import com.mackavn.gettyimages.mvp_interfaces.view.IMainView;
import com.mackavn.gettyimages.views.HistoryImagesAdapter;
import com.mackavn.gettyimages.views.SearchImagesAdapter;

import java.util.Collections;
import java.util.List;

/**
 * Created by mkavu on 08.02.2018.
 */

public class MainPresenter extends BasePresenter<IMainView, IMainModel>
        implements IMainPresenter.View, IMainPresenter.Model{

    private List<ImagesItem> imagesItems;
    private List<HistoryItem> historyItems;
    private SearchImagesAdapter searchImagesAdapter;
    private HistoryImagesAdapter historyImagesAdapter;

    private boolean isExpand;

    public MainPresenter(IMainView view, IMainModel model) {
        super(view, model);
    }

    //region superclass methods

    @Override
    protected void onResume() {
        super.onResume();
        if (getModel() != null) {
            getModel().loadHistory();
        }
    }

    //endregion

    //region view actions
    @Override
    public void onSearchClick(String searchQuery) {
        isExpand = true;
        if (getView() != null) {
            getView().showProgressView();
        }
        if (getModel() != null) {
            getModel().searchImages(searchQuery);
        }
    }

    @Override
    public void onSearchActionsCollapse() {
        if (isExpand && getModel() != null) {
            getModel().loadHistory();
            isExpand = false;
        }
    }

    //endregion

    //region model actions

    @Override
    public void onImagesReceived(GettyItem item, String searchQuery) {
        try {
            getModel().saveSearchHistoryItem(item.getImagesItems().get(0)
                    .getDisplaySizesItems().get(0).getUri(), searchQuery);
        } catch (NullPointerException ignored) {
        } catch (IndexOutOfBoundsException ignored) {
        }
        this.imagesItems = item.getImagesItems();
        searchImagesAdapter = new SearchImagesAdapter(imagesItems);
        if (getView() != null) {
            getView().hideProgress();
            getView().showSearchImages(searchImagesAdapter);
        }
    }

    @Override
    public void onHistoryItemsLoaded(List<HistoryItem> historyItems) {
        Collections.reverse(historyItems);
        this.historyItems = historyItems;
        historyImagesAdapter = new HistoryImagesAdapter(historyItems);
        if (getView() != null) {
            getView().hideProgress();
            getView().showHistoryImages(historyImagesAdapter);
        }
    }

    //endregion
}
