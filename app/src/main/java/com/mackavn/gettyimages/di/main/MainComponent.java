package com.mackavn.gettyimages.di.main;

import com.mackavn.gettyimages.di.BaseComponent;
import com.mackavn.gettyimages.mvp_implementation.view.activities.MainActivity;

import dagger.Subcomponent;

/**
 * Created by mkavu on 08.02.2018.
 */

@MainScope
@Subcomponent(modules = MainModule.class)
public interface MainComponent extends BaseComponent {
    void inject(MainActivity activity);
}
