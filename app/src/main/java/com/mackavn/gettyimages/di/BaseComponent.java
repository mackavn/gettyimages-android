package com.mackavn.gettyimages.di;

/**
 * This is base component for all components. It need for containing components in {@link ComponentsHolder}
 *
 * @author Vladislav Kavunenko on 10/12/17.
 */
public interface BaseComponent {
}
