package com.mackavn.gettyimages.di;

import com.mackavn.gettyimages.di.main.MainComponent;
import com.mackavn.gettyimages.di.main.MainModule;
import com.mackavn.gettyimages.mvp_interfaces.view.IMainView;

import java.util.HashMap;

/**
 * This class for containing dagger components in runtime
 * Created by Mike Diachenko
 * on 04-Oct-17.
 * with dell latitude
 */

public class ComponentsHolder {
    private HashMap<String, BaseComponent> components;
    private AppComponent appComponent;

    public ComponentsHolder(AppComponent appComponent) {
        this.appComponent = appComponent;
        components = new HashMap<>();
    }

    public MainComponent getMainComponentByName(String componentName, IMainView view) {
        if (components != null && components.containsKey(componentName)) {
            return (MainComponent) components.get(componentName);
        }
        MainComponent component = appComponent.plusMainComponent(new MainModule(view));
        components.put(componentName, component);
        return component;
    }

    /**
     * Method returns AppComponent instance
     *
     * @return AppComponent instance
     */
    public AppComponent getAppComponent() {
        return appComponent;
    }

    /**
     * Method removes component from components list
     *
     * @param componentName component's name
     */
    public void cleanComponents(String componentName) {
        if (components != null && components.containsKey(componentName)) {
            components.remove(componentName);
        }
    }
}
