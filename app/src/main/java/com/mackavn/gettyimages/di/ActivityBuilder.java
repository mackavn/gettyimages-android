package com.mackavn.gettyimages.di;

import dagger.Module;

/**
 * List of your app logical modules
 * Separate them by "block" of MVP items.
 */
@Module
public abstract class ActivityBuilder {

}
