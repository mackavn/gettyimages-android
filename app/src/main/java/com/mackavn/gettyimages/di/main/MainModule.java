package com.mackavn.gettyimages.di.main;

import com.mackavn.gettyimages.data.network.api.interfaces.ImagesApiRepository;
import com.mackavn.gettyimages.data.storage.interfaces.HistoryStorageRepository;
import com.mackavn.gettyimages.mvp_implementation.model.MainModel;
import com.mackavn.gettyimages.mvp_implementation.presenter.MainPresenter;
import com.mackavn.gettyimages.mvp_interfaces.model.IMainModel;
import com.mackavn.gettyimages.mvp_interfaces.presenter.IMainPresenter;
import com.mackavn.gettyimages.mvp_interfaces.view.IMainView;

import dagger.Module;
import dagger.Provides;

/**
 * Created by mkavu on 08.02.2018.
 */

@Module
public class MainModule {

    private IMainView view;

    public MainModule(IMainView view) {
        this.view = view;
    }

    @MainScope
    @Provides
    MainPresenter presenter(IMainModel model) {
        return new MainPresenter(view, model);
    }

    /**
     * Return presenter as interface object
     *
     * @return presenter
     */
    @MainScope
    @Provides
    IMainPresenter.View presenterView(MainPresenter presenter) {
        return presenter;
    }

    /**
     * Creating model instance
     *
     * @return model
     */
    @MainScope
    @Provides
    IMainModel model(ImagesApiRepository imagesApiRepository, HistoryStorageRepository historyStorageRepository) {
        return new MainModel(view.getLifecycle(), imagesApiRepository, historyStorageRepository);
    }
}
