package com.mackavn.gettyimages.di;

import com.mackavn.gettyimages.data.NetModule;
import com.mackavn.gettyimages.di.main.MainComponent;
import com.mackavn.gettyimages.di.main.MainModule;

import javax.inject.Singleton;

import dagger.Component;
import dagger.android.AndroidInjectionModule;

/**
 * Base app component. Uses new (2.10+) feature - AndroidInjectionModule.class
 */
@Singleton
@Component(modules = {
        AndroidInjectionModule.class,
        AppModule.class,
        ActivityBuilder.class,
        NetModule.class})
public interface AppComponent {
    MainComponent plusMainComponent(MainModule module);
}
