package com.mackavn.gettyimages.di;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Common app module that provides items, which are necessary in any part of app
 */
@Module
public class AppModule {
    private Application application;

    public AppModule(Application application) {
        this.application = application;
    }

    /**
     * Provides context
     *
     * @return application as Context
     */
    @Provides
    @Singleton
    Context provideContext() {
        return application;
    }

    /**
     * Provides default shared preferences to keep settings or short info
     *
     * @param context provided by dagger2 context instance
     * @return shared preferences instance
     */
    @Provides
    @Singleton
    SharedPreferences provideSharedPreferences(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

}
