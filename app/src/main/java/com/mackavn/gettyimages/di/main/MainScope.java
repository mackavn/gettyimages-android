package com.mackavn.gettyimages.di.main;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by mkavu on 08.02.2018.
 */

@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface MainScope {
}
