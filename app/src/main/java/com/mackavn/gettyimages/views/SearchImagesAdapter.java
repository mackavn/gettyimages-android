package com.mackavn.gettyimages.views;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.mackavn.gettyimages.R;
import com.mackavn.gettyimages.entities.ImagesItem;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by mkavu on 09.02.2018.
 */

public class SearchImagesAdapter extends RecyclerView.Adapter<SearchImagesAdapter.ViewHolder> {

    private List<ImagesItem> imagesItems;

    public SearchImagesAdapter(List<ImagesItem> imagesItems) {
        this.imagesItems = imagesItems;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_item, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (imagesItems.size() <= holder.getAdapterPosition()) {
            return;
        }
        ImagesItem item = imagesItems.get(holder.getAdapterPosition());
        if (item != null) {
            holder.titleTv.setText(item.getTitle());
            holder.imagePb.setVisibility(View.VISIBLE);
            holder.imageIv.setImageDrawable(null);

            Glide.with(holder.imageIv.getContext())
                    .load(item.getDisplaySizesItems().get(0).getUri())
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String s, Target<GlideDrawable> target, boolean b) {
                            holder.imagePb.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable glideDrawable, String s, Target<GlideDrawable> target, boolean b, boolean b1) {
                            holder.imagePb.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .fitCenter()
                    .into(holder.imageIv);
        }
    }

    @Override
    public void onViewRecycled(ViewHolder holder) {
        super.onViewRecycled(holder);
        Glide.clear(holder.imageIv);
        holder.imageIv.setImageDrawable(null);
    }

    @Override
    public int getItemCount() {
        return imagesItems != null ? imagesItems.size() : 0;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv_image) ImageView imageIv;
        @BindView(R.id.tv_title) TextView titleTv;
        @BindView(R.id.pb_image) ProgressBar imagePb;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
