package com.mackavn.gettyimages.data;

/**
 * Use this class to keep base network constants
 */

public class NetworkConstants {
    //default address
    public static final String SERVER_PROD = "https://google.com";
    public static final String SERVER_TEST = "https://test.com";
    public static final String SERVER_URL = SERVER_TEST;
    //network timeouts
    public static final int DEFAULT_SERVER_READ_TIMEOUT = 60;
    public static final int DEFAULT_SERVER_CONNECT_TIMEOUT = 60;
}
