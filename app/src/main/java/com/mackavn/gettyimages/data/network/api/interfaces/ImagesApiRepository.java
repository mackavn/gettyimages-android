package com.mackavn.gettyimages.data.network.api.interfaces;

import com.mackavn.gettyimages.entities.GettyItem;

import io.reactivex.Observable;

/**
 * Created by mkavu on 09.02.2018.
 */

public interface ImagesApiRepository {
    Observable<GettyItem> getImages(String searchQuery);
}
