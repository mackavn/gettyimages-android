package com.mackavn.gettyimages.data.network.api;

import com.mackavn.gettyimages.common.Constants;
import com.mackavn.gettyimages.data.network.api.interfaces.ImagesApiRepository;
import com.mackavn.gettyimages.data.network.api.retrofit.ImagesApiInterface;
import com.mackavn.gettyimages.entities.GettyItem;

import io.reactivex.Observable;
import retrofit2.Retrofit;

/**
 * Created by mkavu on 09.02.2018.
 */

public class ImagesApiManager extends BaseApiManager<ImagesApiInterface> implements ImagesApiRepository {

    public ImagesApiManager(Retrofit retrofit) {
        super(retrofit, ImagesApiInterface.class);
    }

    public Observable<GettyItem> getImages(String searchQuery) {
        return apiInterface.getImages(Constants.GETTY_IMAGES_API_KEY,
                Constants.GETTY_IMAGES_DEFAULT_FIELDS,
                Constants.GETTY_IMAGES_DEFAULT_ORDER,
                searchQuery);
    }
}
