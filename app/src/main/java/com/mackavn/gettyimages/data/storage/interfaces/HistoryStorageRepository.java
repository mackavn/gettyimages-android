package com.mackavn.gettyimages.data.storage.interfaces;

import com.mackavn.gettyimages.entities.HistoryItem;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;

/**
 * Created by mkavu on 09.02.2018.
 */

public interface HistoryStorageRepository {
    Completable saveHistoryItem(HistoryItem historyItem);

    Observable<List<HistoryItem>> getHistoryItems();
}
