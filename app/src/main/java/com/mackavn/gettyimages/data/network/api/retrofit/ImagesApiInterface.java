package com.mackavn.gettyimages.data.network.api.retrofit;

import com.mackavn.gettyimages.common.Constants;
import com.mackavn.gettyimages.entities.GettyItem;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

/**
 * Created by mkavu on 09.02.2018.
 */

public interface ImagesApiInterface {

    @GET(Constants.GETTY_IMAGES_ENDPOINT)
    Observable<GettyItem> getImages(@Header("Api-Key") String apiKey,
                                    @Query("fields") String fields,
                                    @Query("sort_order") String sortOrder,
                                    @Query("phrase") String phrase);
}
