package com.mackavn.gettyimages.data.storage.realmentities;

import io.realm.RealmObject;

/**
 * Created by mkavu on 09.02.2018.
 */

public class RealmHistoryItem extends RealmObject {
    private String searchQuery;
    private String uri;

    public String getSearchQuery() {
        return searchQuery;
    }

    public void setSearchQuery(String searchQuery) {
        this.searchQuery = searchQuery;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }
}
