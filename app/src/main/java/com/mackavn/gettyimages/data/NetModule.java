package com.mackavn.gettyimages.data;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import com.mackavn.gettyimages.BuildConfig;
import com.mackavn.gettyimages.data.network.api.ImagesApiManager;
import com.mackavn.gettyimages.data.network.api.interfaces.ImagesApiRepository;
import com.mackavn.gettyimages.data.storage.HistoryStorageManager;
import com.mackavn.gettyimages.data.storage.interfaces.HistoryStorageRepository;
import com.mackavn.gettyimages.utils.DateTimeUtils;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Dagger2 module that contains base provides for network communications
 */
@Module
public class NetModule {
    private String baseUrl;

    /**
     * @param baseUrl base server url to user with retrofit
     */
    public NetModule(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    // Dagger will only look for methods annotated with @Provides

    /**
     * Here you can pre-build necessary gson object. Include types adapters, gson settings etc.
     *
     * @return builded Gson object
     */
    @Provides
    @Singleton
    Gson provideGson() {
        return new GsonBuilder()
                .setDateFormat(DateTimeUtils.ISO_TIME_FORMAT)
                .create();
    }

    /**
     * Here you can pre-bild necessary OkHttpClien object to user it with retrofit
     *
     * @return builded okhttp client
     */
    @Provides
    @Singleton
    OkHttpClient provideOkHttpClient() {
        OkHttpClient.Builder builder = new OkHttpClient().newBuilder();
        builder.readTimeout(NetworkConstants.DEFAULT_SERVER_READ_TIMEOUT, TimeUnit.SECONDS);
        builder.connectTimeout(NetworkConstants.DEFAULT_SERVER_CONNECT_TIMEOUT, TimeUnit.SECONDS);
        builder.addInterceptor(chain -> {
            Request request = chain.request();
            return chain.proceed(request);
        });
        if (BuildConfig.DEBUG) {
            builder.addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY));
        }
        return builder.build();
    }

    /**
     * Here you can build necessary retrofit object. Use any settings that you need,
     * you can add more dependencies
     *
     * @param gson         - provided with dagger2 builded gson
     * @param okHttpClient provided with dagger2 builded okhttp
     * @return builded retrofit instance
     */
    @Provides
    @Singleton
    Retrofit provideRetrofit(Gson gson, OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }

    /**
     * Here is example to provide ApiRepository
     *
     * @param retrofit - provided with dagger2 builded retrofit
     * @return api repository implementation
     */

    @Provides
    @Singleton
    ImagesApiRepository provideImagesApiManager(Retrofit retrofit) {
        return new ImagesApiManager(retrofit);
    }

    @Provides
    @Singleton
    HistoryStorageRepository provideHistoryStorageManager() {
        return new HistoryStorageManager();
    }
}
