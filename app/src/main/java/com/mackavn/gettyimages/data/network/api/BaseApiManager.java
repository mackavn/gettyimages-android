package com.mackavn.gettyimages.data.network.api;

import retrofit2.Retrofit;

/**
 * Base class to avoid passing class and call "retrofit.create(Class) in repository
 *
 * @param <T> retrofit interface class - look http://square.github.io/retrofit/
 */
abstract class BaseApiManager<T> {

    T apiInterface;

    BaseApiManager(Retrofit retrofit, Class<T> clazz) {
        apiInterface = retrofit.create(clazz);
    }

}
