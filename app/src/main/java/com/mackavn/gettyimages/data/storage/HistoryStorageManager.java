package com.mackavn.gettyimages.data.storage;

import com.mackavn.gettyimages.data.storage.interfaces.HistoryStorageRepository;
import com.mackavn.gettyimages.data.storage.realmentities.RealmHistoryItem;
import com.mackavn.gettyimages.entities.HistoryItem;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.realm.Realm;

/**
 * Created by mkavu on 09.02.2018.
 */

public class HistoryStorageManager implements HistoryStorageRepository {

    @Override
    public Completable saveHistoryItem(HistoryItem historyItem) {
        return Completable.create(e -> {
            try {
                Realm realm = Realm.getDefaultInstance();
                realm.beginTransaction();
                RealmHistoryItem item = new RealmHistoryItem();
                item.setSearchQuery(historyItem.getSearchQuery());
                item.setUri(historyItem.getUri());
                realm.insert(item);
                realm.commitTransaction();
                e.onComplete();
            } catch (Exception e1) {
                e.onError(e1);
            }
        });
    }

    @Override
    public Observable<List<HistoryItem>> getHistoryItems() {
        return Observable.create(e -> {
            try {
                List<RealmHistoryItem> items = Realm.getDefaultInstance()
                        .where(RealmHistoryItem.class)
                        .findAll();
                List<HistoryItem> historyItems = new ArrayList<>();
                for (int i = 0; i < items.size(); i++) {
                    HistoryItem item = new HistoryItem(
                            items.get(i).getSearchQuery(),
                            items.get(i).getUri());
                    historyItems.add(item);
                }
                e.onNext(historyItems);
                e.onComplete();
            } catch (Exception e1) {
                e.onError(e1);
            }
        });
    }
}
