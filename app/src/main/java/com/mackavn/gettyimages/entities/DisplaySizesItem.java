package com.mackavn.gettyimages.entities;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mkavu on 09.02.2018.
 */

public class DisplaySizesItem {
    @SerializedName("uri") private String uri;

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }
}
