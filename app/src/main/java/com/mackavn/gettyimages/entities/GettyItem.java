package com.mackavn.gettyimages.entities;

import com.google.gson.annotations.SerializedName;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by mkavu on 09.02.2018.
 */

public class GettyItem {
    @SerializedName("result_count") private int resultCount;
    @SerializedName("images") private List<ImagesItem> imagesItems;

    public int getResultCount() {
        return resultCount;
    }

    public void setResultCount(int resultCount) {
        this.resultCount = resultCount;
    }

    public List<ImagesItem> getImagesItems() {
        return imagesItems;
    }

    public void setImagesItems(List<ImagesItem> imagesItems) {
        this.imagesItems = imagesItems;
    }
}
