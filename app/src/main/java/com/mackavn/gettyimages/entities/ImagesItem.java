package com.mackavn.gettyimages.entities;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by mkavu on 09.02.2018.
 */

public class ImagesItem {
    @SerializedName("display_sizes") private List<DisplaySizesItem> displaySizesItems;
    @SerializedName("title") private String title;

    public List<DisplaySizesItem> getDisplaySizesItems() {
        return displaySizesItems;
    }

    public void setDisplaySizesItems(List<DisplaySizesItem> displaySizesItems) {
        this.displaySizesItems = displaySizesItems;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
