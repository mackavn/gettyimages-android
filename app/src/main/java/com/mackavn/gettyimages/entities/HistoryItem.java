package com.mackavn.gettyimages.entities;

/**
 * Created by mkavu on 09.02.2018.
 */

public class HistoryItem {
    private String searchQuery;
    private String uri;

    public HistoryItem(String searchQuery, String uri) {
        this.searchQuery = searchQuery;
        this.uri = uri;
    }

    public String getSearchQuery() {
        return searchQuery;
    }

    public void setSearchQuery(String searchQuery) {
        this.searchQuery = searchQuery;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }
}
