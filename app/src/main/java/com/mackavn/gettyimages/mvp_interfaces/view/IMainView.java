package com.mackavn.gettyimages.mvp_interfaces.view;

import com.mackavn.gettyimages.views.HistoryImagesAdapter;
import com.mackavn.gettyimages.views.SearchImagesAdapter;

/**
 * Created by mkavu on 08.02.2018.
 */

public interface IMainView extends IBaseView {
    void showSearchImages(SearchImagesAdapter searchImagesAdapter);

    void showHistoryImages(HistoryImagesAdapter historyImagesAdapter);
}
