package com.mackavn.gettyimages.mvp_interfaces.model;

/**
 * Created by mkavu on 08.02.2018.
 */

public interface IMainModel extends IBaseModel {
    void searchImages(String searchQuery);

    void saveSearchHistoryItem(String uri, String searchQuery);

    void loadHistory();
}
