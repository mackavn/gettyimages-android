package com.mackavn.gettyimages.mvp_interfaces.presenter;

import android.content.Intent;
import android.view.MenuItem;

import com.mackavn.gettyimages.common.AppError;
import com.mackavn.gettyimages.di.AppComponent;
import com.mackavn.gettyimages.mvp_interfaces.view.IBaseView;

/**
 * Created by llaer on 09-Sep-17.
 */

public interface IBasePresenter {
    interface View {

        /**
         * Passed from activity to presenter
         *
         * @param request - request code
         * @param result  - result code (Activity constants OK\CANCEL)
         * @param data    - intent data from result
         */
        void onActivityResult(int request, int result, Intent data);

        /**
         * Passed from activity to presenter
         *
         * @param item - item selected as option
         */
        void onOptionsItemSelected(MenuItem item);

        /**
         * Setting view to presenter
         *
         * @param view view to work in presenter
         */
        void setView(IBaseView view);

        /**
         * clearing data
         */
        void resolve();
    }

    interface Model {

        /**
         * passing error from model to presenter, wrapped in AppError object
         *
         * @param error error wrapped object
         */
        void onError(AppError error);
    }
}
