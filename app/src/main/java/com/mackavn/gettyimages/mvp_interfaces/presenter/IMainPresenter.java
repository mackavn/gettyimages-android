package com.mackavn.gettyimages.mvp_interfaces.presenter;

import com.mackavn.gettyimages.entities.GettyItem;
import com.mackavn.gettyimages.entities.HistoryItem;

import java.util.List;

/**
 * Created by mkavu on 08.02.2018.
 */

public interface IMainPresenter extends IBasePresenter {

    interface View extends IBasePresenter.View {
        void onSearchClick(String searchQuery);

        void onSearchActionsCollapse();
    }

    interface Model extends IBasePresenter.Model {

        void onImagesReceived(GettyItem item, String searchQuery);

        void onHistoryItemsLoaded(List<HistoryItem> historyItems);
    }
}
