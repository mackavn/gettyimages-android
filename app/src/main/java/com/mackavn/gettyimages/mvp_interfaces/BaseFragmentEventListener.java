package com.mackavn.gettyimages.mvp_interfaces;

import android.content.Intent;

import com.mackavn.gettyimages.mvp_implementation.view.fragments.BaseFragment;

/**
 * base fragment event listener, used to avoid long passing activity\intent\fragment launchs
 */
public interface BaseFragmentEventListener {
    void launchActivity(Class clzz, boolean isFinish);

    void launchIntent(Intent intent, boolean finish);

    void openFragment(BaseFragment fragment, boolean addToBackStack);

}
