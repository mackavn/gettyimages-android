package com.mackavn.gettyimages.mvp_interfaces.view;

import android.arch.lifecycle.Lifecycle;
import android.content.Intent;

import com.mackavn.gettyimages.common.AppError;
import com.mackavn.gettyimages.di.AppComponent;
import com.mackavn.gettyimages.mvp_implementation.view.fragments.BaseFragment;

/**
 * Created by Mike Diachenko
 * on 09-Sep-17.
 * with Alienware
 */

public interface IBaseView {
    /**
     * Shows any error in view (can do nothing) - from DB\network loading or any other failure
     *
     * @param error custom project error class that contains code, message and throwable
     */
    void showError(AppError error);

    /*
     * showProgressBar() shows progress bar (default). Has custom controls in base class
     * showProgressView() shows progress view (any view).
     * showEmptyView() shows empty view (any view).
     * hideProgress() hide all progress
     * all method null-safety
     */
    void showProgressBar();

    void showProgressView();

    void showEmptyView();

    void hideEmptyView();

    void hideProgress();

    /*
     * lockUI() set flag about locked UI in view to true
     * isUILocked() return current flag state
     * unlockUI() set flag about locked UI in view to false
     */
    void lockUI();

    boolean isUILocked();

    void unlockUI();

    /**
     * Method  used to start activity for result from presenter and pass results to that presenter
     * by using callback onActivityResult in IBasePresenter.View
     *
     * @param intent intent to launch
     * @param code code to process
     */
    void startActivityForResult(Intent intent, int code);

    /**
     * Launchs activity from presenter, with ability to kill current activity
     *
     * @param clzz   class to launch
     * @param finish if true - kills activity
     */
    void launchActivity(Class clzz, boolean finish);

    /**
     * Launchs intent from presenter, with ability to kill current activity
     *
     * @param intent intent to launch
     * @param finish if true - kills activity
     */
    void launchIntent(Intent intent, boolean finish);

    /**
     * Showing fragment, with ability add it to stack
     *
     * @param fragment   fragment to show
     * @param addToStack if true - add to stack
     */
    void openFragment(BaseFragment fragment, boolean addToStack);

    /**
     * Unique tag used for components and may be other things
     *
     * @return string tag
     */
    String getUniqueTag();

    /**
     * Used to provide lifecycle for view and model
     *
     * @return lifecycle object (usually this)
     */
    Lifecycle getLifecycle();
}
