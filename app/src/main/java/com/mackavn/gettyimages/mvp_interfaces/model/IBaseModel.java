package com.mackavn.gettyimages.mvp_interfaces.model;

import com.mackavn.gettyimages.mvp_interfaces.presenter.IBasePresenter;

/**
 * Created by llaer on 09-Sep-17.
 */

public interface IBaseModel {
    void setPresenter(IBasePresenter.Model presenter);
    void resolve();
}
