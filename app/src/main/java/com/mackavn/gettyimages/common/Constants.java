package com.mackavn.gettyimages.common;

/**
 * Use this class to keep any common constans
 */
public final class Constants {
    public static final String GETTY_IMAGES_API_KEY = "c3vscj2dc5unchwyax2qs8a4";
    public static final String GETTY_IMAGES_DEFAULT_FIELDS = "id,title,thumb";
    public static final String GETTY_IMAGES_DEFAULT_ORDER = "best";
    public static final String GETTY_IMAGES_ENDPOINT = "https://api.gettyimages.com/v3/search/images";
}

