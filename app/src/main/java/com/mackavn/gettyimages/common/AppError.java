package com.mackavn.gettyimages.common;

/**
 * Base project error wrapper. You can add\remove any fields or methods in it
 */

public class AppError {
    private int code;
    private String message;
    private Throwable throwable;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Throwable getThrowable() {
        return throwable;
    }

    public void setThrowable(Throwable throwable) {
        this.throwable = throwable;
    }
}
