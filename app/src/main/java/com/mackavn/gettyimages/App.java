package com.mackavn.gettyimages;

import android.app.Application;

import com.mackavn.gettyimages.data.NetModule;
import com.mackavn.gettyimages.data.NetworkConstants;
import com.mackavn.gettyimages.di.AppComponent;
import com.mackavn.gettyimages.di.AppModule;
import com.mackavn.gettyimages.di.ComponentsHolder;
import com.mackavn.gettyimages.di.DaggerAppComponent;

import io.realm.Realm;


/**
 * Created by mertsimsek on 25/05/2017.
 */

public class App extends Application {
    private AppComponent appComponent;
    private ComponentsHolder componentsHolder;

    @Override
    public void onCreate() {
        super.onCreate();
        appComponent = DaggerAppComponent
                .builder()
                .appModule(new AppModule(this))
                .netModule(new NetModule(NetworkConstants.SERVER_URL))
                .build();
        Realm.init(this);
        componentsHolder = new ComponentsHolder(appComponent);
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }

    public ComponentsHolder getComponentsHolder() {
        return componentsHolder;
    }
}
